#!/bin/bash

cat product/app/GoogleCameraGo/GoogleCameraGo.apk.* 2>/dev/null >> product/app/GoogleCameraGo/GoogleCameraGo.apk
rm -f product/app/GoogleCameraGo/GoogleCameraGo.apk.* 2>/dev/null
cat product/app/webview/webview.apk.* 2>/dev/null >> product/app/webview/webview.apk
rm -f product/app/webview/webview.apk.* 2>/dev/null
cat system_ext/priv-app/Settings/Settings.apk.* 2>/dev/null >> system_ext/priv-app/Settings/Settings.apk
rm -f system_ext/priv-app/Settings/Settings.apk.* 2>/dev/null
