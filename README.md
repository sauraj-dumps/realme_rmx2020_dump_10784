## full_oppo6769-user 10 QP1A.190711.020 68b77aba7cb33275 release-keys
- Manufacturer: realme
- Platform: mt6768
- Codename: RMX2020
- Brand: realme
- Flavor: aospa_RMX2020-userdebug
- Release Version: 12
- Id: SKQ1.220201.001
- Incremental: eng.lordsa.20220226.190427
- Tags: test-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-US
- Screen Density: undefined
- Fingerprint: google/coral/coral:12/SQ1A.220205.002/8010174:user/release-keys
- OTA version: 
- Branch: full_oppo6769-user-10-QP1A.190711.020-68b77aba7cb33275-release-keys
- Repo: realme_rmx2020_dump_10784


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
